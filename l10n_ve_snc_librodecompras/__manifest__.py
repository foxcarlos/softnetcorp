# -*- coding: utf-8 -*-

{
    'name': "Libro de Compras - Requerimientos Contables",

    'summary': """
        Libro de Compras
        """,

    'description': """
        Libro de Compras
    """,

    'author': "SoftNet Corp",
    'website': "https://softnetcorp.net/",

    'category': 'Localization',
    'version': '12.0.0.2',

    # any module necessary for this one to work correctly
     "depends" : ['base','account','l10n_ve_snc_retiva'],

    # always loaded
    'data': [
        'views/wizard_libro_compras.xml',
        'reports/report_factura_proveedores.xml'],  
    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
}

