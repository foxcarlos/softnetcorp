# -*- coding: utf-8 -*-
import csv
import io
import base64
from datetime import datetime, timedelta

from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError
import openerp.addons.decimal_precision as dp
import logging

from io import StringIO
import xlsxwriter
import shutil

#from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx

_logger = logging.getLogger(__name__)

def rif_format(valor):
    if valor: 
        return valor.replace('-','')
    return '0'

def tipo_format(valor):
    if valor and valor=='in_refund': 
        return '03'
    return '01'

def float_format(valor):
    if valor:
        result = '{:,.2f}'.format(valor)
        #_logger.info('Result 1: %s' % result)
        result = result.replace(',','')
        #_logger.info('Result 2: %s' % result)
        return result
    return valor

class BsoftContratoReport2(models.TransientModel):
    _name = 'snc.wizard.retencioniva'
    _description = 'Generar archivo TXT de retenciones de IVA'
       
    delimiter = '\t'
    quotechar = "'"
    date_from = fields.Date(string='Fecha de Llegada', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))       
    date_to = fields.Date(string='Fecha de Salida', default=lambda *a:(datetime.now() + timedelta(days=(1))).strftime('%Y-%m-%d'))
    file_data = fields.Binary('Archivo TXT', filters=None, help="",)
    file_name = fields.Char('Nombre de archivo', size=256, required=False, help="",)   
    
    @api.multi
    def show_view(self, name, model, id_xml, res_id=None, view_mode='tree,form', nodestroy=True, target='new'):
        context = self._context
        mod_obj = self.env['ir.model.data']
        view_obj = self.env['ir.ui.view']
        module = ""
        view_id = self.env.ref(id_xml).id
        if view_id:
            view = view_obj.browse(view_id)
            view_mode = view.type
        ctx = context.copy()
        ctx.update({'active_model': model})
        res = {'name': name,
                'view_type': 'form',
                'view_mode': view_mode,
                'view_id': view_id,
                'res_model': model,
                'res_id': res_id,
                'nodestroy': nodestroy,
                'target': target,
                'type': 'ir.actions.act_window',
                'context': ctx,
                }
        return res

    
    @api.multi
    def action_generate_txt(self):                                     
        tax_group_id = (
            self.env["account.tax.group"].search([("is_islr", "!=", False)]).id
        )        
        dominio = [('manual','=',True),
                   ('invoice_id.type','in',('in_invoice', 'in_refund')),
                   ('invoice_id.amount_retiva','!=',0.00)]
        
        if self.date_from:
            dominio.append(('invoice_id.date','>=',self.date_from))    

        if self.date_to:
            dominio.append(('invoice_id.date','<=',self.date_to))       
                    
        rec_ids = self.env['account.invoice.tax'].search(dominio,order='number_retiva').ids                                   
        rec_cursor = self.env['account.invoice.tax'].browse(rec_ids)
        
        #output = io.BytesIO()
        output = io.StringIO()
        writer = csv.writer(output, delimiter=self.delimiter, quotechar=self.quotechar, quoting=csv.QUOTE_NONE)
        
        company = self.env['res.company']._company_default_get('account.invoice')
        rif = rif_format(company.vat)
        mydict = []
        for rec in rec_cursor:
            #periodo = rec.periodo            
            if ('Retencion de IVA' in rec.name) and (rec.number_retiva) and (rec.amount!=0):# (rec.retiva_id and rec.retiva_id.id):
                exento = 0
                retenciones = 0
                base_imponible = 0
                for impu in rec.invoice_id.tax_line_ids:
                    if impu.amount==0:
                        exento += impu.base
                    if not (impu.amount>0):
                        retenciones += impu.amount
                    if (impu.amount>0): 
                        base_imponible += impu.base
                        
                periodo = '%s'%(rec.invoice_id.date)
                periodo = periodo.replace('-', '')
                periodo = periodo[0:6]
                #exento = 0 #abs(rec.total_factura-rec.base_imponible-rec.impuesto_iva-rec.amount)
                total = base_imponible+exento+rec.invoice_id.amount_tax
                alicuota = abs(rec.invoice_id.amount_tax/base_imponible*100)
                por_reten = abs(rec.amount/rec.invoice_id.amount_tax*100)            
                #total = rec.base_imponible+rec.impuesto_iva+exento
                por_iva = rec.impuesto_iva/base_imponible*100
                fecha = rec.invoice_id.date_invoice
                su_rif = rif_format(rec.invoice_id.partner_id.vat)
                refer = rec.invoice_id.refund_invoice_id.supplier_invoice_number if rec.tipo=='in_refund' and rec.invoice_id.refund_invoice_id else '0'
                lin_01 = [
                    rif,
                    periodo,
                    fecha,
                    'C',
                    tipo_format(rec.tipo),
                    su_rif,  
                    rec.invoice_id.supplier_invoice_number,
                    rec.invoice_id.supplier_invoice_control,   
                    float_format(abs(total)),
                    float_format(abs(base_imponible)),
                    float_format(abs(rec.amount)),
                    refer,           
                    rec.number_retiva,
                    float_format(exento),
                    float_format(por_iva),
                    '0'
                    ]             

                writer.writerow(lin_01)             
      
        encoded = output.getvalue()
        if encoded:
            encoded = base64.b64encode(encoded.encode('utf-8'))
            self.write({'file_data': encoded,
                        'file_name': "Retenciones de IVA desde %s hasta %s.txt"%(self.date_from,self.date_to),
                        })     
            return self.show_view('Arcivo Generado', self._name, 'l10n_ve_snc_retiva.snc_wizard_retencioniva_form_view2', self.id)
        raise UserError(_("Nothing to print."))
    
    