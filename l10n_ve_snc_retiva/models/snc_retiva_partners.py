# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from odoo import models, fields, api, _

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)

# mapping invoice type to journal type
TYPE2JOURNAL = {
    #'out_invoice': 'sale',
    #'in_invoice': 'purchase',
    'wh_iva_out_invoice': 'sale',
    'wh_iva_in_invoice': 'purchase',    
    #'out_refund': 'sale',
    #'in_refund': 'purchase',
}

# mapping invoice type to refund type
TYPE2REFUND = {
    'out_invoice': 'out_refund',        # Customer Invoice
    'in_invoice': 'in_refund',          # Vendor Bill
    'out_refund': 'out_invoice',        # Customer Refund
    'in_refund': 'in_invoice',          # Vendor Refund
}

class SncRetivaPartners(models.Model):
    _name = "snc.retiva.partners"
    
    _description = "Retencion IVA de clientes"
    
    def _get_default_journal(self):
        journal_domain = [
            ('type', '=', 'wh_iva_out_invoice'),
            ('company_id', '=', self.env.user.company_id.id),
        ]
        default_journal_id = self.env['account.journal'].search(journal_domain, limit=1)
        return default_journal_id
       
    name = fields.Char(string='Numero de Comprobante',size=14, required=True)
    partner_id = fields.Many2one('res.partner',string='Agente de Retención')
    company_id = fields.Many2one('res.company', string='Sujeto Retenido',  required=True, default=lambda self: self.env['res.company']._company_default_get('account.invoice'))    
    fecha_comprobante = fields.Date(string='Fecha de Comprobante', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    fecha_contabilizacion = fields.Date(string='Fecha de Contabilizacion', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))    
    retiva_line = fields.One2many('snc.retiva.partners.lines','retiva_partner_id',string='Lineas de Comprobante', copy=False, ondelete='restrict')
    move_id = fields.Many2one('account.move',string='Asiento')
    state = fields.Selection(related='move_id.state',string='Estatus', store=True, default='draft')
    journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Journal',
        default=_get_default_journal,        
        help='Set default journal to use on Withholding IVA'
    )    
        
    _sql_constraints = [
     ('unique_comprobante', 'unique(partner_id, name)', 'You can not use a voucher number twice for the same customer!\n Please enter a different voucher number')
        ]    
    
    @api.multi
    @api.onchange('name')
    def _onchange_name(self):
        if self.name:   
            if not (len(self.name)==14):
                raise UserError(_('La longitud de campo numero de comprobante debe ser 14 caracteres'))
            val = self.name
            if not val.isdigit(): 
                raise UserError(_('El campo numero de comprobante solo puede contener caracteres numericos'))                   
    
    @api.multi
    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.retiva_line = []
        if self.partner_id.retiva_id:
            reti = self.env['snc.retiva.partners.lines'].search([('partner_id', '=', self.partner_id.id),('retiva_partner_id','!=',False)])
            inv_vec = [x.invoice_id.id for x in reti]
            domain = [('partner_id', '=', self.partner_id.id),('type', 'in', ['out_invoice','out_refund']),('state','=','open')]
            if inv_vec:
                domain.append(('id', 'not in', inv_vec))
            inv = self.env['account.invoice'].search(domain)
            #inv = self.env['account.invoice'].search([('partner_id', '=', self.partner_id.id),('type', 'in', ['out_invoice','out_refund']),('number_retiva','=',False),('state','=','open')])
            facturas = []
            for fac in inv:
                base_imponible = fac.amount_tax
                if base_imponible!=0:                
                    retiva = self.partner_id.retiva_id
                    monto_sujeto, importe_retenido = retiva.get_retencion(base_imponible)
                    porc_retener = 0
                    if monto_sujeto>0:
                        porc_retener = round(importe_retenido/monto_sujeto*100,2)
                    datos = {'invoice_id':fac.id,
                             'retiva_id':retiva.id}
                    facturas.append((0, 0, datos))
                #facturas.append(lin.id)
            #retiva_line = [(6, 0, facturas)] 
            if facturas:
                self.retiva_line = facturas  
                values = {
                    'retiva_line': facturas,
                }
            #self.update(values)
        
    @api.model
    def _set_journal(self):
        return self.journal_id
        
    @api.multi
    def prepare_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        move_line = self.env["account.move.line"]
        for inv in self:
            
            journal = self._set_journal()
    
            # List of move.line 
            ctx = dict(self._context, lang=inv.partner_id.lang)
            
            if not inv.fecha_contabilizacion:
                inv.with_context(ctx).write({'fecha_contabilizacion': fields.Date.context_today(self)})            
            date = inv.fecha_contabilizacion
            company_currency = inv.company_id.currency_id
            
            line = []
            total = 0
            for linea in inv.retiva_line:
                move_id = linea.invoice_id.move_id.id
                total += linea.importe_retenido
                _logger.info('ID: %s - Invoice ID: %s - Invoice Nro: %s'%(linea.id,linea.invoice_id.id,linea.invoice_id.number))
                datos = {
                    'name': 'Retencion IVA Fact. %s'%linea.invoice_id.number, # a label so accountant can understand where this line come from
                    'debit': (abs(linea.importe_retenido) if linea.importe_retenido>0 else 0.00), # amount of debit
                    'credit': (abs(linea.importe_retenido) if linea.importe_retenido<0 else 0.00), # amount of credit
                    'account_id': inv.partner_id.property_account_receivable_id.id, # account 
                    'date': date,
                    "ref": linea.invoice_id.number,
                    'partner_id': inv.partner_id.id, # partner if there is one  
                    "company_id": linea.invoice_id.company_id.id,
                    "invoice_id": linea.invoice_id.id,  
                    "quantity": 1,
                    "move_id": move_id,
                    'journal_id': journal.id,
                    'reconciled':False,                                    
                    }
                line.append((0,0,datos))
                #move_line.with_context(check_move_validity=False).create(datos)                
                datos = {
                    'name': 'Retencion IVA Nro. %s'%inv.name, # a label so accountant can understand where this line come from
                    'debit': (abs(linea.importe_retenido) if linea.importe_retenido<0 else 0.00), # amount of debit
                    'credit': (abs(linea.importe_retenido) if linea.importe_retenido>0 else 0.00), # amount of credit
                    'account_id': linea.account_id.id, # account
                    "ref": linea.invoice_id.number,                 
                    'date': date,
                    "company_id": linea.invoice_id.company_id.id,
                    'partner_id': inv.partner_id.id, # partner if there is one
                    "invoice_id": linea.invoice_id.id,
                    "quantity": 1,
                    "move_id": move_id,
                    'journal_id': journal.id,
                    'reconciled':False,                    
                    }
                _logger.info('Retencion de IVA Cliente %s Nro. %s'%(inv.partner_id.name,inv.name))
                line.append((0,0,datos))
                #move_line.with_context(check_move_validity=False).create(datos)                    
            
            move_vals = {
                'ref': 'Ret.IVA No. %s'%inv.name,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': 'Retencion de IVA Cliente %s Nro. %s'%(inv.partner_id.name,inv.name)
            }
            return move_vals                   
    
    @api.multi
    def write(self,vals):
        for record in self:
            for line in record.retiva_line:
                line.invoice_id.write({
                    'number_retiva':'',
                    'input_retiva':False,
                    'retiva_id':False})
        result = super(SncRetivaPartners,self).write(vals)
        #print('Result:',result)
        if result: 
            for record in self:
                for line in record.retiva_line:
                    line.invoice_id.write({
                        'number_retiva':record.name,
                        'input_retiva':line.importe_retenido,
                        'retiva_id':line.retiva_id.id})                 
        return result
    
    @api.model
    def create(self,values):
        record = super(SncRetivaPartners,self).create(values)   
        if record:
            data_account = record.prepare_move_create()
            move_id = self.env['account.move'].with_context(check_move_validity=False).create(data_account)
            move_id.write({'ref':'Ret.IVA No. %s'%record.name})
            move_id.post()
            record.write({'move_id':move_id.id})
        return record 
    
    @api.multi 
    def unlink(self):
        for record in self:
            #self.move_id.unlink()
            if self.move_id:
                self.move_id.button_cancel()
                self.move_id.unlink()
            for line in record.retiva_line:
                line.invoice_id.write({
                    'number_retiva':'',
                    'input_retiva':False,
                    'retiva_id':False})        
        result = super(SncRetivaPartners,self).unlink()
        return result          


class SncRetivaPartnersLines(models.Model):
    _name = "snc.retiva.partners.lines"
    
    _description = "Retencion IVA de clientes facturas"    
    
    @api.one
    @api.depends('retiva_id', 'invoice_id')
    def _compute_retiva(self):
        base_imponible = self.invoice_id.amount_tax
        retiva = self.retiva_id
        if retiva:
            monto_sujeto, importe_retenido = retiva.get_retencion(base_imponible)
            porc_retener = 0
            if monto_sujeto!=0:
                porc_retener = round(abs(importe_retenido/monto_sujeto*100),2)        
            self.monto_sujeto = monto_sujeto        
            self.porc_retener = porc_retener
            self.importe_retenido = importe_retenido
            if self.invoice_id.type=='out_invoice':
                self.account_id = retiva.account_id  
            else: 
                self.account_id = retiva.account_refund_id    
    
    retiva_partner_id = fields.Many2one('snc.retiva.partners', required=True, ondelete='cascade', index=True, copy=False, readonly=True)
    invoice_id = fields.Many2one('account.invoice','Numero de Factura')
    partner_id = fields.Many2one(related="invoice_id.partner_id")
    monto_sujeto = fields.Float(string='Monto Sujeto',
        store=True, readonly=True, compute='_compute_retiva')          
    porc_retener = fields.Float(string='Porcentaje a Retener')
    importe_retenido = fields.Float(string='Importe Retenido',
        store=True, readonly=True, compute='_compute_retiva')              
    retiva_id = fields.Many2one('snc.retiva',string='Retencion de IVA')
    account_id = fields.Many2one('account.account',compute='_compute_retiva',string='Cuenta de Impuesto',help="Seleccionar cuenta contable que ira a asiento en contabilidad")  
    
    