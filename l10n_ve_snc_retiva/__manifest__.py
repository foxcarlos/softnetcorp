# -*- coding: utf-8 -*-
{
    'name': "Retención IVA - Requerimientos Contables",

    'summary': """
        Retencion IVA
        """,

    'description': """
        Retencion IVA
    """,

    'author': "SoftNet Corp",
    'website': "https://softnetcorp.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Localization',
    'version': '12.0.0.2',

    # any module necessary for this one to work correctly
    'depends': ['account','web','report_xlsx','base_vat','l10n_ve_snc_rif','l10n_ve_snc_invoice','l10n_ve'],

    # always loaded
    'data': [        
        'views/snc_retiva_views.xml',
        'views/snc_retiva_partners_views.xml',
        'views/res_partner_views.xml',
        'views/account_invoice_views.xml',
        'data/snc_retiva_data.xml',
        'wizards/wizard_generar_txt_view.xml',
        'views/snc_external_layout.xml',
        'views/snc_footer.xml',
        'views/account_invoice_retiva_template.xml',
        'report/account_invoice_report.xml',
        'data/snc_retiva_action_data.xml',   
        'security/ir.model.access.csv',     
    ],
    "post_init_hook": "post_init_hook",    
    # only loaded in demonstration mode
    'demo': [
    ],
}