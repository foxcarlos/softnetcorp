# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    supplier_invoice_number = fields.Char(
        string='Vendor Invoice Number',
        readonly=True,
        states={'draft': [('readonly', False)]},
        copy=False)
    
    supplier_invoice_control = fields.Char(
        string='Vendor Invoice Control',
        readonly=True,
        states={'draft': [('readonly', False)]},
        copy=False)
    
    invoice_number_control = fields.Char(
        string='Invoice NumberControl',
        readonly=True,
        states={'draft': [('readonly', False)]},
        copy=False)        
    control_number_next = fields.Char(string='Next Control Number', compute="_get_control_number_next", inverse="_set_control_next")
    control_number_next_prefix = fields.Char(string='Next Control Number Prefix', compute="_get_control_prefix")
    control_sequence_enable = fields.Boolean(related="journal_id.control_sequence")
    
    def _get_control_prefix(self):
        """ computes the prefix of the number that will be assigned to the first invoice/bill/refund of a journal, in order to
        let the user manually change it.
        """
        if not self.env.user._is_system():
            for invoice in self:
                invoice.sequence_number_next_prefix = False
                invoice.sequence_number_next = ''
            return
        for invoice in self:
            journal_sequence, domain = invoice._get_control_number_next_stuff()
            if (invoice.state == 'draft') and not self.search(domain, limit=1):
                prefix, dummy = journal_sequence.with_context(ir_sequence_date=invoice.date_invoice,
                                                              ir_sequence_date_range=invoice.date_invoice)._get_prefix_suffix()
                invoice.control_number_next_prefix = prefix
            else:
                invoice.control_number_next_prefix = False

    @api.depends('state', 'journal_id')
    def _get_control_number_next(self):
        """ computes the number that will be assigned to the first invoice/bill/refund of a journal, in order to
        let the user manually change it.
        """
        for invoice in self:
            journal_sequence, domain = invoice._get_control_number_next_stuff()
            if (invoice.state == 'draft') and not self.search(domain, limit=1):
                number_next = journal_sequence._get_current_sequence().number_next_actual
                invoice.control_number_next = '%%0%sd' % journal_sequence.padding % number_next
            else:
                invoice.control_number_next = ''

    @api.multi
    def _set_control_next(self):
        ''' Set the number_next on the sequence related to the invoice/bill/refund'''
        self.ensure_one()
        journal_sequence, domain = self._get_control_number_next_stuff()
        if not self.env.user._is_admin() or not self.control_number_next or self.search_count(domain):
            return
        nxt = re.sub("[^0-9]", '', self.control_number_next)
        result = re.match("(0*)([0-9]+)", nxt)
        if result and journal_sequence:
            # use _get_current_sequence to manage the date range sequences
            sequence = journal_sequence._get_current_sequence()
            sequence.number_next = int(result.group(2))
        
    def _get_control_number_next_stuff(self):
        self.ensure_one()
        journal_sequence = self.journal_id.control_sequence_id
        if self.journal_id.control_sequence:
            domain = [('type', '=', self.type)]
            journal_sequence = self.type in ['in_refund', 'out_refund'] and self.journal_id.control_sequence_id or self.journal_id.sequence_id
        elif self.type in ['in_invoice', 'in_refund']:
            domain = [('type', 'in', ['in_invoice', 'in_refund'])]
        else:
            domain = [('type', 'in', ['out_invoice', 'out_refund'])]
        if self.id:
            domain += [('id', '<>', self.id)]
        domain += [('journal_id', '=', self.journal_id.id), ('state', 'not in', ['draft', 'cancel'])]
        return journal_sequence, domain

    @api.constrains('supplier_invoice_number')
    def _check_unique_supplier_invoice_number_insensitive(self):
        """
        Check if an other vendor bill has the same supplier_invoice_number
        and the same commercial_partner_id than the current instance
        """
        for rec in self:
            if rec.supplier_invoice_number and\
                    rec.type in ('in_invoice', 'in_refund'):
                same_supplier_inv_num = rec.search([
                    ('commercial_partner_id', '=',
                     rec.commercial_partner_id.id),
                    ('type', 'in', ('in_invoice', 'in_refund')),
                    ('supplier_invoice_number',
                     '=ilike', rec.supplier_invoice_number),
                    ('id', '!=', rec.id)
                ], limit=1)
                if same_supplier_inv_num:
                    raise ValidationError(_(
                        "The invoice/refund with supplier invoice number '%s' "
                        "already exists in Odoo under the number '%s' "
                        "for supplier '%s'.") % (
                            same_supplier_inv_num.supplier_invoice_number,
                            same_supplier_inv_num.number or '-',
                            same_supplier_inv_num.partner_id.display_name))
                    
    @api.constrains('supplier_invoice_control')
    def _check_unique_supplier_invoice_control_insensitive(self):
        """
        Check if an other vendor bill has the same supplier_invoice_number
        and the same commercial_partner_id than the current instance
        """
        for rec in self:
            if rec.supplier_invoice_control and\
                    rec.type in ('in_invoice', 'in_refund'):
                same_supplier_inv_num = rec.search([
                    ('commercial_partner_id', '=',
                     rec.commercial_partner_id.id),
                    ('type', 'in', ('in_invoice', 'in_refund')),
                    ('supplier_invoice_control',
                     '=ilike', rec.supplier_invoice_control),
                    ('id', '!=', rec.id)
                ], limit=1)
                if same_supplier_inv_num:
                    raise ValidationError(_(
                        "The invoice/refund with supplier invoice control number '%s' "
                        "already exists in Odoo under the control number '%s' "
                        "for supplier '%s'.") % (
                            same_supplier_inv_num.supplier_invoice_control,
                            same_supplier_inv_num.number or '-',
                            same_supplier_inv_num.partner_id.display_name))
                    

    @api.onchange('supplier_invoice_number')
    def _onchange_supplier_invoice_number(self):
        if not self.reference:
            self.reference = self.supplier_invoice_number

    @api.onchange('reference')
    def _onchange_vendor_bill_reference(self):
        if self.type in ['in_invoice', 'in_refund'] \
                and not self.supplier_invoice_number:
            self.supplier_invoice_number = self.reference

    @api.model
    def _prepare_refund(self, invoice, date_invoice=None,
                        date=None, description=None, journal_id=None):
        """
        The unique vendor invoice number cannot be passed to the credit note
        in vendor bills
        """
        vals = super()._prepare_refund(
            invoice, date_invoice, date, description, journal_id)

        if invoice and invoice.type in ['in_invoice', 'in_refund'] and\
                'reference' in vals:
            vals['reference'] = ''

        return vals

    @api.multi
    def copy(self, default=None):
        """
        The unique vendor invoice number is not copied in vendor bills
        """
        if self.type in ['in_invoice', 'in_refund']:
            default = dict(default or {}, reference='')
        return super().copy(default)
    
    @api.multi
    def action_move_create(self):
        for inv in self:
            journal = inv.journal_id
            if not inv.invoice_number_control and journal.control_sequence:
                sequence = journal.control_sequence_id
                new_name = sequence.with_context(ir_sequence_date=inv.date_invoice).next_by_id()
                inv.write({'invoice_number_control': new_name})
        return super(AccountInvoice,self).action_move_create()    
    
class AccountJournal(models.Model):
    _inherit = 'account.journal'
    
    control_sequence_id = fields.Many2one('ir.sequence', string='Control Number Entry Sequence',
        help="This field contains the information related to the numbering of control entries of this journal.", copy=False)    
    control_sequence = fields.Boolean(string='Dedicated Control Sequence', help="Check this box if you don't want to share the same sequence for invoices and control number made from this journal", default=False)
    control_sequence_number_next = fields.Integer(string='Control Number: Next Number',
        help='The next sequence number will be used for the next control number.',
        compute='_compute_control_seq_number_next',
        inverse='_inverse_control_seq_number_next')
    
    @api.multi
    # do not depend on 'refund_sequence_id.date_range_ids', because
    # refund_sequence_id._get_current_sequence() may invalidate it!
    @api.depends('control_sequence_id.use_date_range', 'control_sequence_id.number_next_actual')
    def _compute_control_seq_number_next(self):
        '''Compute 'sequence_number_next' according to the current sequence in use,
        an ir.sequence or an ir.sequence.date_range.
        '''
        for journal in self:
            if journal.control_sequence_id and journal.control_sequence:
                sequence = journal.control_sequence_id._get_current_sequence()
                journal.control_sequence_number_next = sequence.number_next_actual
            else:
                journal.control_sequence_number_next = 1

    @api.multi
    def _inverse_control_seq_number_next(self):
        '''Inverse 'control_sequence_number_next' to edit the current sequence next number.
        '''
        for journal in self:
            if journal.control_sequence_id and journal.control_sequence and journal.control_sequence_number_next:
                sequence = journal.control_sequence_id._get_current_sequence()
                sequence.number_next = journal.control_sequence_number_next
                
    @api.multi
    def write(self, vals):
        result = super(AccountJournal, self).write(vals)
        if vals.get('control_sequence'):
            for journal in self.filtered(lambda j: j.type in ('sale', 'purchase') and not j.control_sequence_id):
                journal_vals = {
                    'name': journal.name,
                    'company_id': journal.company_id.id,
                    'code': journal.code,
                    'control_sequence_number_next': vals.get('control_sequence_number_next', journal.control_sequence_number_next),
                }
                journal.control_sequence_id = self.with_context(control_number=True).sudo()._create_sequence(journal_vals, refund=False).id        
        return result
    
    @api.model
    def create(self, vals):
        if vals.get('type') in ('sale', 'purchase') and vals.get('control_sequence') and not vals.get('control_sequence_id'):
            vals.update({'control_sequence_id': self.with_context(control_number=True).sudo()._create_sequence(vals, refund=False).id})
        return super(AccountJournal, self).create(vals)     
    
    
class IrSequence(models.Model):
    _inherit = 'ir.sequence'
        
    @api.model
    def create(self, vals):
        if self._context.get('control_number'):
            seq_name = vals['name'] + _(': Control Number')
            vals.update({'name': seq_name}) 
        return super(IrSequence, self).create(vals)            