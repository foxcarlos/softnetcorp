# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Número de Control Facturas - Requerimientos Contables',
    "version": "12.0.0.2",
    'summary': 'Personalizar numero de factura y agregar numero de control',

    'description': """
Personalizar numero de factura y agregar numero de control
    """,
    'author': 'SoftNet Corp',
    'collaborator': 'SoftNet Corp',
    'category': 'Localization',
    'website': 'https://softnetcorp.net',
    'depends' : ['base','account'],
    'data': [
	    'views/account_invoice_view.xml'
    ],

    'installable': True,
    'application': False,
    'auto_install': False,

}
