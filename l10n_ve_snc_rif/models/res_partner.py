# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Partner(models.Model):
    _inherit = 'res.partner'

    rif = fields.Char(string='RIF/CI',size=12, help="Indicar numero RIF o Cedula de Identidad.")

    @api.onchange('rif','vat')
    def solo_mayusculas(self):            
        if self.rif:                    
            self.rif = str(self.rif).upper()
        if self.vat:                    
            self.vat = str(self.vat).upper()             

    @api.constrains("rif")
    def check_rif(self):

        if len(self.rif) > 12 or len(self.rif) < 1:
            raise ValidationError("Ingrese un campo identificacion válido")
            return False
        tipo = self.rif[0:1]
        valor = self.rif[1:]
        if tipo not in ['J','G','V','E','P','C']:
            raise ValidationError("Debe Ingresar un dato valido V,E,P,J,G,C: %s-%s"%(tipo,valor))
            return False
        if not valor.isdigit():
            raise ValidationError("Debe Ingresar un dato numerico %s"%valor)
            return False                    
        self.vat = "%s%s"%(tipo,valor)
        
    @api.constrains("vat")
    def check_vat(self):

        if len(self.vat) > 12 or len(self.vat) < 1:
            raise ValidationError("Ingrese un campo identificacion válido")
            return False
        tipo = self.vat[0:1]
        valor = self.vat[1:]
        if tipo not in ['J','G','V','E','P','C']:
            raise ValidationError("Debe Ingresar un dato valido V,E,P,J,G,C: %s-%s"%(tipo,valor))
            return False
        if not valor.isdigit():
            raise ValidationError("Debe Ingresar un dato numerico %s"%valor)
            return False                            
        
