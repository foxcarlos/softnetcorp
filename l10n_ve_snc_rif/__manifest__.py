# -*- coding: utf-8 -*-
{
    'name' : 'Rif de Compañía/Clientes/Proveedores - Requerimientos Contables',
    'version': '12.0.0.2',
    'description':
"""
Rif de Compañía/Clientes/Proveedores
====================================
Se requiere en la creación de un Cliente/Proveedor, en los Módulos que involucre su creación tales como
Contabilidad, Ventas, Compras, Inventarios, entre otros; en formulario Cliente/Proveedor, crear campo llamado
“Rif”, campo editable, que estará compuesto por:
- Doce (12) dígitos, no permitir más de dicha cantidad.
- Ejemplo: J-30963872-6.
- El primer digito solo permitirá colocar las siguientes letras
    Para empresas: J – G
    Para no empresas: V - E
-Este campo debe estar ubicado sobre los campos de “Dirección”.
Mover campo de “Etiquetas” a la parte derecha del formulario.
    """,
    'author': 'SoftNetCorp',
    'collaborator': 'SoftNetCorp',
    'category': 'Localization',
    'website': 'https://softnetcorp.net',
    'depends' : ['base','account','l10n_ve'],
    'data': [
        'views/res_partner_view.xml',
	    'views/res_company_view.xml',
	    'views/account_invoice_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
