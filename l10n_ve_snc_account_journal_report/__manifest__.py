# -*- coding: utf-8 -*-
{
    'name': "Reporte Libro Diario",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': 'SoftNet Corp',
    'collaborator': 'SoftNet Corp',

    'category': 'Localization',
    "version": "12.0.0.2",

    # any module necessary for this one to work correctly
    "depends" :['account','l10n_ve_snc_retiva','report_xlsx'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/report_general_daily.xml',
        'views/account_report.xml',        
        'wizard/account_report_general_daily_view.xml',        
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
