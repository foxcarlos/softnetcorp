# -*- coding: utf-8 -*-

{
    'name': "Resumen_declaracion_IVA",

    'summary': """
        Resumen Declaracion IVA
        """,

    'description': """
        Resumen Declaracion IVA
    """,

    'author': "SoftNet Corp",
    'website': "https://softnetcorp.net/",


    'category': 'Localization',
    'version': '12.0.0.2',

    # any module necessary for this one to work correctly
     "depends" : ['base','account'],

    # always loaded
    'data': [
        'views/wizard_declaracion_iva.xml',
        'reports/report_resumen_declaracion_iva.xml'],  
    # only loaded in demonstration mode
    'demo': [
    ],
}


