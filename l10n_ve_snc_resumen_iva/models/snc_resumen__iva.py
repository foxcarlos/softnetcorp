# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from odoo import models, fields, api, _

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)

# mapping invoice type to journal type
TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale',
    'in_refund': 'purchase',
}

# mapping invoice type to refund type
TYPE2REFUND = {
    'out_invoice': 'out_refund',        # Customer Invoice
    'in_invoice': 'in_refund',          # Vendor Bill
    'out_refund': 'out_invoice',        # Customer Refund
    'in_refund': 'in_invoice',          # Vendor Refund
}

class SncRetivaPartners(models.Model):
    _name = "snc.resumen.iva"
    
    _description = "Retencion IVA de clientes"
       
    name = fields.Char(string='Titulo',size=200, required=True)
    fecha_inicial = fields.Date(string='Fecha Inicial', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    fecha_final = fields.Date(string='Fecha Final', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))    
    doc_out_ids = fields.Many2one('account.invoice',string='Ventas',domain=[('type','in',('out_invoice', 'out_refund'))])
    doc_in_ids = fields.Many2one('account.invoice',string='Compras',domain=[('type','in',('in_invoice', 'in_refund'))])
    state = fields.Selection(related='move_id.state',string='Estatus', store=True, default='draft')
    sum_01_basimp = fields.Float(string=' 1 Ventas Internas no Gravadas - Base Imponible', digits=dp.get_precision('Product Price'))
    sum_03_basimp = fields.Float(string=' 3 Ventas Internas Gravadas por Alicuota General - Base Imponible', digits=dp.get_precision('Product Price'))
    sum_03_impues = fields.Float(string=' 3 Ventas Internas Gravadas por Alicuota General - Débito Fiscal', digits=dp.get_precision('Product Price'))
    sum_06_basimp = fields.Float(string=' 6 Total Ventas y Debitos Fiscales para Efectos de Determinación - Base Imponible', digits=dp.get_precision('Product Price'))
    sum_06_impues = fields.Float(string=' 6 Total Ventas y Debitos Fiscales para Efectos de Determinación - Débito Fiscal', digits=dp.get_precision('Product Price'))
    
       
