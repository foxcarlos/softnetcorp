# -*- coding: utf-8 -*-

{
    'name': "Libro de Ventas - Requerimientos Contables",

    'summary': """
        Libro de Ventas
        """,

    'description': """
        Libro de ventas
    """,

    'author': "SoftNet Corp",
    'website': "https://softnetcorp.net/",

    'category': 'Localization',
    'version': '12.0.0.2',

    # any module necessary for this one to work correctly
     "depends" : ['base','account','l10n_ve_snc_retiva'],

    # always loaded
    'data': [
        'views/wizard_libro_ventas.xml',
        'reports/report_factura_clientes.xml'],  
    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
}
