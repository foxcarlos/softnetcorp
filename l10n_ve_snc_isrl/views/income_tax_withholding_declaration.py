# -*- coding: utf-8 -*-
# Copyright 2019 Yan Chirino <ychirino@intechmultiservicios.com>
import base64
from odoo import _, fields, models, api  # noqa
from odoo.exceptions import UserError
from lxml import etree

import logging

_logger = logging.getLogger(__name__)


class IncomeTaxWithholdingDeclaration(models.TransientModel):
    _name = "income.tax.withholding.declaration"
    _description = "Income Tax Withholding Declaration in Venezuela (ISLR)"

    date_from = fields.Date(string="Date from")
    date_to = fields.Date(string="Date to")
    file_data = fields.Binary('Archivo XML', filters=None, help="",)
    file_name = fields.Char('Nombre de archivo', size=256, required=False, help="",)    

    def _check_values(self):
        if self.date_from > self.date_to:
            raise UserError(_("The start date can not be longer than the end date."))

        return True

    def print_isrl_xml_report(self):

        if self._check_values():
            tax_group_id = (
                self.env["account.tax.group"].search([("name", "=", "Withholdings")]).id
            )
            ids = (
                self.env["account.invoice.tax"]
                .search(
                    [
                        ("tax_id.tax_group_id", "=", tax_group_id),
                        ("create_date", ">=", self.date_from),
                        ("create_date", "<=", self.date_to),
                    ]
                )
                .ids
            )

            if ids:
                self = self.with_context(active_ids=ids, active_model="account.invoice.tax")
                data = {'form': {}}
                data['ids'] = self.env.context.get('active_ids', [])
                data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
                _logger.info('Form Reporte %s', data)
                islr_obj = self.env['account.invoice.tax'].browse(ids)
                root = etree.Element('RelacionRetencionesISLR', Periodo="201709", RifAgente="J309638726")
                for reten in islr_obj:
                    header = etree.SubElement(root,'DetalleRetencion')
                    child = etree.SubElement(header,'RifRetenido')  
                    child.text = reten.invoice_id.partner_id.rif 
                    root.append(child)
                    child = etree.SubElement(header,'NumeroFactura') 
                    child.text = reten.invoice_id.supplier_invoice_number
                    root.append(child)
                    child = etree.SubElement(header,'NumeroControl') 
                    child.text = reten.invoice_id.supplier_invoice_control 
                    root.append(child)
                    child = etree.SubElement(header,'FechaOperacion') 
                    child.text = '%s'%reten.invoice_id.date_invoice 
                    root.append(child)
                    child = etree.SubElement(header,'CodigoConcepto') 
                    child.text = reten.tax_id.description 
                    root.append(child)
                    child = etree.SubElement(header,'MontoOperacion') 
                    child.text = '%s'%reten.subject_amount 
                    root.append(child)
                    child = etree.SubElement(header,'PorcentajeRetencion') 
                    child.text = '%s'%reten.tax_id.amount
                    root.append(child)
                    
                xlm_str = str(etree.tostring(root,  encoding='utf-8', xml_declaration = True, pretty_print = True).decode())
                                                
                #return self.env.ref('l10n_ve_snc_isrl.report_action_set_withholdings_isrl').report_action(self, data=data)
                
                encoded = xlm_str #base64.encodestring(xlm_str)
                _logger.info('XML Reporte %s', xlm_str)
                #self.write({'file_data': encoded,
                #            'file_name': "Retenciones de ISLR desde %s hasta %s.txt"%(self.date_from,self.date_to),
                #            })                   
                return self.show_view('Arcivo Generado', self._name, 'l10n_ve_snc_islr.view_income_tax_withholding_declaration_form', self.id)
                
                #return (
                #    self.env["report"]
                #    .with_context(active_ids=ids, active_model="account.invoice.tax")
                #    .get_action([], "l10n_ve_snc_isrl.set_withholdings_isrl_report")
                #)

            raise UserError(_("Nothing to print."))
        
class IslrXmlReport(models.AbstractModel):
    _name = "report.l10n_ve_snc_isrl.set_withholdings_isrl_report"
   
    @api.model
    def _get_report_values(self, docids, data=None):
        ids = data['ids']
        cr = self._cr
        query = """select so.name as sale_sequence,so.amount_total as total_amount,rp.name as sales_person_name
            from account.invoice.tax so
            where so.id in ['%s']""" % (ids)
        cr.execute(query)
        dat = cr.dictfetchall()       
        return {
           'start_date': start_date,
           'end_date': end_date,
           'dat': dat,
           }        
