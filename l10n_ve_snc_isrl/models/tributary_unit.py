# -*- coding: utf-8 -*-
# Copyright 2019 Yan Chirino <ychirino@intechmultiservicios.com>

from odoo import fields, models, _  # noqa


class TributaryUnit(models.Model):
    _name = "tributary.unit"
    _description = "Tributary Unit in Venezuela"

    name = fields.Char(string=_("Official Gazette Nº"))
    date = fields.Date(string=_("Gazette Date"))
    amount = fields.Float(string=_("Amount"))
