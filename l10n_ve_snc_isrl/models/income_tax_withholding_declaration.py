# -*- coding: utf-8 -*-
# Copyright 2019 Yan Chirino <ychirino@intechmultiservicios.com>
import io
import base64
from datetime import datetime, timedelta

from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError
import openerp.addons.decimal_precision as dp
from lxml import etree
from io import StringIO

import logging

_logger = logging.getLogger(__name__)

def rif_format(valor):
    if valor: 
        return valor.replace('-','')
    return '0'

class IncomeTaxWithholdingDeclaration(models.TransientModel):
    _name = "income.tax.withholding.declaration"
    _description = "Income Tax Withholding Declaration in Venezuela (ISLR)"

    date_from = fields.Date(string="Date from")
    date_to = fields.Date(string="Date to")
    file_data = fields.Binary('Archivo XML', filters=None, help="",)
    file_name = fields.Char('Nombre de archivo', size=256, required=False, help="",)    

    def _check_values(self):
        if self.date_from > self.date_to:
            raise UserError(_("The start date can not be longer than the end date."))

        return True
    
    @api.multi
    def show_view(self, name, model, id_xml, res_id=None, view_mode='tree,form', nodestroy=True, target='new'):
        context = self._context
        mod_obj = self.env['ir.model.data']
        view_obj = self.env['ir.ui.view']
        module = ""
        view_id = self.env.ref(id_xml).id
        if view_id:
            view = view_obj.browse(view_id)
            view_mode = view.type
        ctx = context.copy()
        ctx.update({'active_model': model})
        res = {'name': name,
                'view_type': 'form',
                'view_mode': view_mode,
                'view_id': view_id,
                'res_model': model,
                'res_id': res_id,
                'nodestroy': nodestroy,
                'target': target,
                'type': 'ir.actions.act_window',
                'context': ctx,
                }
        return res    

    def print_isrl_xml_report(self):

        if self._check_values():
            tax_group_id = (
                self.env["account.tax.group"].search([("is_islr", "!=", False)]).id
            )
            ids = (
                self.env["account.invoice.tax"]
                .search(
                    [
                        ("tax_id.tax_group_id", "=", tax_group_id),
                        ("create_date", ">=", self.date_from),
                        ("create_date", "<=", self.date_to),
                    ]
                )
                .ids
            )

            if ids:
                islr_obj = self.env['account.invoice.tax'].browse(ids)
                periodo = '%s'%(self.date_to)
                periodo = periodo.replace('-', '')
                periodo = periodo[0:6]
                company = self.env['res.company']._company_default_get('account.invoice')
                rif = rif_format(company.vat)
                root = etree.Element('RelacionRetencionesISLR', Periodo=periodo, RifAgente=rif)
                for reten in islr_obj:
                    header = etree.SubElement(root,'DetalleRetencion')
                    child = etree.SubElement(header,'RifRetenido')  
                    child.text = reten.invoice_id.partner_id.vat 
                    header.append(child)
                    child = etree.SubElement(header,'NumeroFactura') 
                    child.text = reten.invoice_id.supplier_invoice_number
                    header.append(child)
                    child = etree.SubElement(header,'NumeroControl') 
                    txt = reten.invoice_id.supplier_invoice_control
                    _logger.info('TXT %s', txt)
                    ltxt = txt.split('-')
                    _logger.info('SLIPT TXT %s', ltxt)
                    _logger.info('LEN SLIPT TXT %s', len(ltxt))
                    if len(ltxt)>1:
                        txt = ltxt[1]  
                    child.text = txt #reten.invoice_id.supplier_invoice_control 
                    header.append(child)
                    child = etree.SubElement(header,'FechaOperacion') 
                    fecha = '%s'%(reten.invoice_id.date_invoice)
                    fecha = fecha.replace('-', '')
                    yy = fecha[0:4]
                    mm = fecha[4:6]
                    dd = fecha[6:]
                    child.text = '%s/%s/%s'%(dd,mm,yy) 
                    header.append(child)
                    child = etree.SubElement(header,'CodigoConcepto') 
                    child.text = reten.tax_id.description 
                    header.append(child)
                    child = etree.SubElement(header,'MontoOperacion') 
                    child.text = '%s'%reten.subject_amount 
                    header.append(child)
                    child = etree.SubElement(header,'PorcentajeRetencion') 
                    child.text = '%s'%reten.tax_id.amount
                    header.append(child)                    
                xlm_str = str(etree.tostring(root,  encoding='utf-8', xml_declaration = True, pretty_print = True).decode())
                encoded = xlm_str 
                encoded = base64.b64encode(encoded.encode('utf-8'))
                self.write({'file_data': encoded,
                            'file_name': "Retenciones de ISLR desde %s hasta %s.xml"%(self.date_from,self.date_to),
                            })                   
                return self.show_view('Arcivo Generado', self._name, 'l10n_ve_snc_isrl.view_income_tax_withholding_declaration_form2', self.id)
                
            raise UserError(_("Nothing to print."))
        
class IslrXmlReport(models.AbstractModel):
    _name = "report.l10n_ve_snc_isrl.set_withholdings_isrl_report"
   
    @api.model
    def _get_report_values(self, docids, data=None):
        ids = data['ids']
        cr = self._cr
        query = """select so.name as sale_sequence,so.amount_total as total_amount,rp.name as sales_person_name
            from account.invoice.tax so
            where so.id in ['%s']""" % (ids)
        cr.execute(query)
        dat = cr.dictfetchall()       
        return {
           'start_date': start_date,
           'end_date': end_date,
           'dat': dat,
           }        
