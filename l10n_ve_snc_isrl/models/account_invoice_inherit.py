# -*- coding: utf-8 -*-
# Copyright 2019 Yan Chirino <ychirino@intechmultiservicios.com>

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError

import logging

_logger = logging.getLogger(__name__)


class AccountInvoiceTaxInherit(models.Model):
    _inherit = "account.invoice.tax"

    subject_amount = fields.Float(string=_("Subject Amount"))

    subject_amount_total = fields.Float(related="subject_amount", readonly=True)
    withholding = fields.Boolean(string="Withholdings", default=False)
    date = fields.Date(string="Date")


class AccountInvoiceLineInherit(models.Model):
    _inherit = "account.invoice.line"

    income_tax = fields.Many2many("account.tax", string="Retenciones")

    @api.onchange("product_id")
    def onchange_product_id(self):
        partner_taxes = self.partner_id.income_tax
        if partner_taxes:
            withholdings = []
            for tax in partner_taxes:
                if tax.type_tax_use == "purchase":
                    withholdings.append(tax.id)
            return {"domain": {"income_tax": [("id", "in", withholdings)]}}  # noqa


class AccountInvoiceInherit(models.Model):
    _inherit = "account.invoice"

    tax_withholdings = fields.Monetary(
        compute='_compute_amount', string="Retenciones fiscales"
    )

    total_taxes = fields.Monetary(compute='_compute_amount', string="Tax")
    total_retiva = fields.Monetary(compute='_compute_amount', string="Total Impuesto Retenido")
    
    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice','input_retiva','invoice_line_ids.income_tax')
    def _compute_amount(self):
        super(AccountInvoiceInherit,self)._compute_amount()        
        self.tax_withholdings = 0
        self.total_taxes = 0
        self.total_retiva = 0            
        round_curr = self.currency_id.round
        self.tax_withholdings = sum(round_curr(line.amount_total) for line in self.tax_line_ids if line.manual and line.tax_id.person_type)
        self.total_retiva = self.amount_retiva + self.tax_withholdings         
        self.total_taxes = self.amount_tax
        if self.type in ['in_invoice','in_refund']:
            self.amount_total = self.amount_untaxed + self.amount_tax + self.total_retiva
        else:
            self.amount_total = self.amount_untaxed + self.amount_tax
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed        
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id
            amount_total_company_signed = currency_id._convert(self.amount_total, self.company_id.currency_id, self.company_id, self.date_invoice or fields.Date.today())
            amount_untaxed_signed = currency_id._convert(self.amount_untaxed, self.company_id.currency_id, self.company_id, self.date_invoice or fields.Date.today())
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign                    
    
    def _prepare_islr_line_vals(self, line, tax):        
        vals = {
            'invoice_id': self.id,
            'name': tax['name'],
            'tax_id': tax['id'],
            'amount': tax['amount'],
            'base': tax['base'],
            'subject_amount': tax['subject_amount'],
            #'subject_amount_total': tax['base'],
            'manual': True,
            'sequence': tax['sequence'],
            'account_analytic_id': tax['analytic'] and line.account_analytic_id.id or False,
            'account_id': self.type in ('out_invoice', 'in_invoice') and (tax['account_id'] or line.account_id.id) or (tax['refund_account_id'] or line.account_id.id),
            'analytic_tag_ids': tax['analytic'] and line.analytic_tag_ids.ids or False,
        }
        if not vals.get('account_analytic_id') and line.account_analytic_id and vals['account_id'] == line.account_id.id:
            vals['account_analytic_id'] = line.account_analytic_id.id
        return vals      
    
    def _prepare_retiva_line_vals(self, base_imponible):
        values = super(AccountInvoiceInherit,self)._prepare_retiva_line_vals(base_imponible)
        if not values.get('subject_amount') and values.get('base'):
            values['subject_amount'] = values['base']
        #if not values.get('subject_amount_total') and values.get('base'):
        #    values['subject_amount_total'] = values['base']           
        return values      
    
    def _prepare_tax_line_vals(self, line, tax):
        values = super(AccountInvoiceInherit,self)._prepare_tax_line_vals(line, tax)
        if not values.get('subject_amount') and values.get('base'):
            values['subject_amount'] = values['base']
        #if not values.get('subject_amount_total') and values.get('base'):
        #    values['subject_amount_total'] = values['base']        
        return values    
    
    @api.multi
    def get_taxes_values(self):        
        tax_grouped = super(AccountInvoiceInherit,self).get_taxes_values()
        for line in self.invoice_line_ids:
            if line.account_id:
                if line.income_tax:                                
                    price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.income_tax.compute_all(price_unit, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
                    for tax in taxes:
                        val = self._prepare_islr_line_vals(line, tax)
                        key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
                        if key not in tax_grouped:
                            tax_grouped[key] = val
                        else:
                            tax_grouped[key]['amount'] += val['amount']
                            tax_grouped[key]['base'] += val['base']               
        return tax_grouped    

    @api.multi
    def print_isrl_retention(self):
        if self.tax_withholdings >= 0:
            raise UserError(_("Nothing to print."))
        self = self.with_context(active_ids=self.ids, active_model="account.invoice")
        return self.env.ref('l10n_ve_snc_isrl.action_report_isrl').report_action(self)

class AccountJournalIslr(models.Model):
    _inherit = "account.journal"    
    
    type = fields.Selection(selection_add=[('wh_islr_in_invoice', 'Retencion ISLR Compras'),
                                                    ('wh_islr_out_invoice', 'Retencion ISLR Ventas')])
    
        
    @api.multi
    def open_action(self):
        """return action based on type for related journals"""
        if self.type in ['wh_islr_in_invoice','wh_islr_out_invoice']: 
            return False        
        return super(AccountJournalIslr,self).open_action()
    
