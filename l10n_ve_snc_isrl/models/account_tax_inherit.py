# -*- coding: utf-8 -*-
# Copyright 2019 Yan Chirino <ychirino@intechmultiservicios.com>

from odoo import fields, models, api, _

import logging

_logger = logging.getLogger(__name__)


class AccountTaxInherit(models.Model):
    _inherit = "account.tax"

    person_type = fields.Selection(
        selection=(
            [
                ("PNR", _("Resident Natural Person")),
                ("PNNR", _("Non-Resident Natural Person")),
                ("LPD", _("Legal Person Domiciled")),
                ("LPND", _("Legal Person Not Domiciled")),
            ]
        ),
        string="Person Type",
        help="Select the type of person if the group of taxes is withholdings",
    )

    @api.multi
    def compute_all(self, price_unit, currency=None, quantity=1.0, product=None, partner=None):
        if not self.person_type:            
            return super(AccountTaxInherit,self).compute_all(price_unit, currency, quantity, product, partner)        
    
        if len(self) == 0:
            company_id = self.env.user.company_id
        else:
            company_id = self[0].company_id
        if not currency:
            currency = company_id.currency_id
        taxes = []
        prec = currency.decimal_places

        round_tax = False if company_id.tax_calculation_rounding_method == 'round_globally' else True
        round_total = True
        if 'round' in self.env.context:
            round_tax = bool(self.env.context['round'])
            round_total = bool(self.env.context['round'])

        if not round_tax:
            prec += 5

        total_excluded = total_included = base = round(price_unit * quantity, prec)

        for tax in self.sorted(key=lambda r: r.sequence):           
            tax_amount = -self._tax_withholdings(price_unit)
            tax_base =base

            taxes.append({
                'id': tax.id,
                'name': tax.with_context(**{'lang': partner.lang} if partner else {}).name,
                'amount': tax_amount,
                'base': tax_base,
                'subject_amount': tax_base,
                'sequence': '100',
                'account_id': tax.account_id.id,
                'refund_account_id': tax.refund_account_id.id,
                'analytic': tax.analytic,
                'price_include': tax.price_include,
                'tax_exigibility': tax.tax_exigibility,
                'tax_id':tax.id,
                'manual':True,
            })

        return {
            'taxes': sorted(taxes, key=lambda k: k['sequence']),
            'total_excluded': currency.round(total_excluded) if round_total else total_excluded,
            'total_included': currency.round(total_included) if round_total else total_included,
            'base': base,
        }

    
    def _tax_withholdings(self, base):
        if self.person_type == "PNR":
            factor = 83.3334
            try:
                uvt = self.env["tributary.unit"].search([], order="date desc")[0].amount
            except BaseException:
                raise ValidationError(
                    _("No value has been " "configured for the tax unit (UVT)")
                )
            retention_percentage = self.amount
            subtract = uvt * (retention_percentage / 100) * factor
            isrl = base * (retention_percentage / 100) - subtract
            if isrl < 0:
                isrl = -isrl
            return isrl

        else:
            retention_percentage = self.amount
            isrl = base * (retention_percentage / 100)
            return isrl
        
class AccountTaxGroup(models.Model):
    _inherit = "account.tax.group"
    
    is_islr = fields.Boolean("Es ISLR?", default=False)
    
class AccountTaxTemplate(models.Model):
    _inherit = "account.tax.template"    

    person_type = fields.Selection(
        selection=(
            [
                ("PNR", _("Resident Natural Person")),
                ("PNNR", _("Non-Resident Natural Person")),
                ("LPD", _("Legal Person Domiciled")),
                ("LPND", _("Legal Person Not Domiciled")),
            ]
        ),
        string="Person Type",
        help="Select the type of person if the group of taxes is withholdings",
    )                
    
    def _get_tax_vals(self, company, tax_template_to_tax):
        val = super(AccountTaxTemplate,self)._get_tax_vals(company, tax_template_to_tax)
        if self.person_type:
            val['person_type'] = self.person_type
        return val
    
    
        