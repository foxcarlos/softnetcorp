from lxml import etree

root = etree.Element('RelacionRetencionesISLR', Periodo="201709", RifAgente="J309638726")
header = etree.SubElement(root,'DetalleRetencion')
child = etree.SubElement(header,'RifRetenido')  
child.text = 'V114880490' 
root.append(child)
child = etree.SubElement(header,'NumeroFactura') 
child.text = '27'
root.append(child)
child = etree.SubElement(header,'NumeroControl') 
child.text = '27' 
root.append(child)
child = etree.SubElement(header,'FechaOperacion') 
child.text = '30/09/2017' 
root.append(child)
child = etree.SubElement(header,'CodigoConcepto') 
child.text = '001' 
root.append(child)
child = etree.SubElement(header,'MontoOperacion') 
child.text = '2000000' 
root.append(child)
child = etree.SubElement(header,'PorcentajeRetencion') 
child.text = '9.04' 
root.append(child)

s = str(etree.tostring(root,  encoding='utf-8', xml_declaration = True, pretty_print = True).decode())
print(s)

